package steps;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class SearchVendor {
	
	WebDriver driver;
	@Given("open Browser")
	public void openBrowser() {
	    
System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		
		//Launch chrome
		driver= new ChromeDriver();	
	}

	@Given("enter Url")
	public void enterUrl() {
		driver.get("https://acme-test.uipath.com/account/login");
		}

	@Given("maximize Browser")
	public void maximizeBrowser() {
	
		driver.manage().window().maximize();
	
	}

	@Given("set Timeout")
	public void setTimeout() {
		driver.manage().timeouts().implicitlyWait(2000,TimeUnit.MILLISECONDS);
		
	}

	@Given("Enter username (.*)")
	public void enterUsernameNishanthimayaaGmailCom(String Uname) {
		driver.findElement(By.id("email")).sendKeys(Uname);
	}

	@Given("Enter password (.*)")
	public void enterPasswordShanthi(String pwd) {
		driver.findElement(By.id("password")).sendKeys(pwd);
	}

	@Given("Click on Login")
	public void clickOnLogin() {
		driver.findElement(By.id("buttonLogin")).click();
		
	}

	@Given("mouseOver on Vendors")
	public void mouseoverOnVendors() {
WebElement eleVendor = driver.findElement(By.xpath("//button[text()=' Vendors']"));
		
		Actions act=new Actions(driver);
		act.moveToElement(eleVendor).perform();
	    
	}

	@Given("click on searchForVendors")
	public void clickOnSearchForVendors() throws InterruptedException {
	 
		Thread.sleep(1000);
		driver.findElement(By.linkText("Search for Vendor")).click();
	}

	@Given("enter VendorID (.*)")
	public void enterVendorIDRO(String vendorID) {
		
    driver.findElement(By.id("vendorTaxID")).sendKeys(vendorID);
		
		
		

	}

	@When("Click on Search button")
	public void clickOnSearchButton() {
		//click on search
				driver.findElement(By.id("buttonSearch")).click();
	}

	@Then("getVendorName")
	public void getvendorname() {

		//Navigate to table
		WebElement eleTable = driver.findElement(By.xpath("//table[@class='table']"));
		
		 List<WebElement> rows = eleTable.findElements(By.tagName("tr"));
		 
		 WebElement eleFirstRow = rows.get(1);
		 
		 List<WebElement> cells = eleFirstRow.findElements(By.tagName("td"));
		 //get Venodr
		 String txtVendor = cells.get(0).getText();
		 System.out.println("Vendor name : "+ txtVendor);
	}




}
